import java.util.InputMismatchException;
import java.util.Scanner;

public class Triunghi implements InterfataAriePerimetru {

	double dimensiuneLatura1 = 0, dimensiuneLatura2 = 0, dimensiuneLatura3 = 0;

	private double citireDate() {

		try {
			Scanner s = new Scanner(System.in);
			double valoare = s.nextDouble();

			while (valoare <= 0) {
				System.out.print("Dimensiunea trebuie sa fie > 0! Reintroduceti: ");
				valoare = s.nextDouble();
			}
			return valoare;
		} catch (InputMismatchException e) {
			System.out.print("Trebuie sa introduceti un numar! Reintroduceti: ");
			return -1;
		}
	}

	protected void initializareLaturi() {
		System.out.print("Introduceti dimensiunea laturei 1 a triunghiului: ");
		
		dimensiuneLatura1 = citireDate();
		while (dimensiuneLatura1 == -1) {
			dimensiuneLatura1 = citireDate();
		}

		System.out.print("Introduceti dimensiunea laturei 2 a triunghiului: ");
		
		dimensiuneLatura2 = citireDate();
		while (dimensiuneLatura2 == -1) {
			dimensiuneLatura2 = citireDate();
		}

		System.out.print("Introduceti dimensiunea laturei 3 a triunghiului: ");
		
		dimensiuneLatura3 = citireDate();
		while (dimensiuneLatura3 == -1) {
			dimensiuneLatura3 = citireDate();
		}
	}

	@Override
	public double calculArie() {
		System.out.println("");
		System.out.println("~Calcul Arie Triunghi Oarecare~");
		double arieTriunghi = 0;
		double semiPerimetru;

		initializareLaturi();

		while (arieTriunghi == 0) {
			if (((dimensiuneLatura1 + dimensiuneLatura2) > dimensiuneLatura3)	&& ((dimensiuneLatura1 + dimensiuneLatura3) > dimensiuneLatura2)	&& ((dimensiuneLatura2 + dimensiuneLatura3) > dimensiuneLatura1))
			{
				System.out.println("Dimensiunile introduse pot forma un triunghi!");

				semiPerimetru = (dimensiuneLatura1 + dimensiuneLatura2 + dimensiuneLatura3) / 2;
				arieTriunghi = Math.sqrt(semiPerimetru * (semiPerimetru - dimensiuneLatura1)* (semiPerimetru - dimensiuneLatura2) * (semiPerimetru - dimensiuneLatura3));
			}
			else
			{
				System.out.println("Dimensiunile introduse nu pot forma un triunghi! Reintroduceti!");
				System.out.println("");
				initializareLaturi();
			}
		}
		System.out.println("Aria Triunghiului Oarecare este: " + arieTriunghi);
		return arieTriunghi;
	}

	@Override
	public double calculPerimetru() {
		System.out.println("");
		System.out.println("~Calcul Perimetru Triunghi Oarecare~");
		double perimetruTriunghi = 0;

		initializareLaturi();

		while (perimetruTriunghi == 0) {
			if (((dimensiuneLatura1 + dimensiuneLatura2) > dimensiuneLatura3)	&& ((dimensiuneLatura1 + dimensiuneLatura3) > dimensiuneLatura2)	&& ((dimensiuneLatura2 + dimensiuneLatura3) > dimensiuneLatura1))
			{
				System.out.println("Dimensiunile introduse pot forma un triunghi!");

				perimetruTriunghi = (dimensiuneLatura1 + dimensiuneLatura2 + dimensiuneLatura3)/3;
			}
			else
			{
				System.out.println("Dimensiunile introduse nu pot forma un triunghi! Reintroduceti!");
				initializareLaturi();
			}
		}
		System.out.println("Perimetrul Triunghiului Oarecare este: " + perimetruTriunghi);
		return perimetruTriunghi;
	}
}
