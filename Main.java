import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	public static void main(String args[]) {

		int ok = 1;

		while (ok == 1) {

			Scanner s1 = new Scanner(System.in);
			int rel;

			System.out.print("Alegeti figura pentru calcularea ariei si a perimetrului: 1-Patrat  2-Cerc  3-Triunghi  4-Dreptunghi :  ");

			try {
				rel = s1.nextInt();

				while (rel <= 0) {
					System.out.print("Reintroduceti : 1-Patrat  2-Cerc  3-Triunghi  4-Dreptunghi :  ");

					rel = s1.nextInt();
				}

				while (rel == 1) {
					Patrat patrat1 = new Patrat();
					patrat1.calculArie();
					patrat1.calculPerimetru();

					System.out.print("Doriti sa reluati pentru patrat? 1-Da  Altfel-Iesire :  ");
					rel = s1.nextInt();

					if (rel != 1) {
						ok = 0;
						System.out.println("Program incheiat pentru ca nu au fost respectate cerintele sau pentru ca ati dorit dumneavoastra! Reluati!");
					}
				}

				while (rel == 2) {
					Cerc cerc1 = new Cerc();
					cerc1.calculArie();
					cerc1.calculPerimetru();

					System.out.print("Doriti sa reluati pentru cerc? 2-Da  Altfel-Iesire :  ");
					rel = s1.nextInt();

					if (rel != 2) {
						ok = 0;
						System.out.println("Program incheiat pentru ca nu au fost respectate cerintele sau pentru ca ati dorit dumneavoastra! Reluati!");
					}
				}

				while (rel == 3) {
					Triunghi triunghi1 = new Triunghi();
					triunghi1.calculArie();
					triunghi1.calculPerimetru();

					System.out.print("Doriti sa reluati pentru triunghi? 3-Da  Altfel-Iesire :  ");
					rel = s1.nextInt();

					if (rel != 3) {
						ok = 0;
						System.out.println("Program incheiat pentru ca nu au fost respectate cerintele sau pentru ca ati dorit dumneavoastra! Reluati!");
					}
				}
				
				while (rel == 4) {
					Dreptunghi dreptunghi1 = new Dreptunghi();
					dreptunghi1.calculArie();
					dreptunghi1.calculPerimetru();

					System.out.print("Doriti sa reluati pentru dreptunghi? 4-Da  Altfel-Iesire :  ");
					rel = s1.nextInt();

					if (rel != 4) {
						ok = 0;
						System.out.println("Program incheiat pentru ca nu au fost respectate cerintele sau pentru ca ati dorit dumneavoastra! Reluati!");
					}
				}
				
			} catch (InputMismatchException e)
			{
				System.out.println("Program incheiat pentru ca nu au fost " + "respectate cerintele sau pentru ca"
						+ " ati dorit dumneavoastra! Reluati!");
				ok = 0;
			}
		}
	}
}