import java.util.InputMismatchException;
import java.util.Scanner;

public class Patrat implements InterfataAriePerimetru {

	private double citireDate() {

		try {
			Scanner s = new Scanner(System.in);
			System.out.print("Introduceti dimensiunea laturei patratului: ");
			double valoare = s.nextDouble();

			while (valoare <= 0) {
				System.out.print("Dimensiunea trebuie sa fie > 0 ! Reintroduceti: ");
				valoare = s.nextDouble();
			}
			return valoare;
		} catch (InputMismatchException e) 
		{
			System.out.println("Trebuie sa introduceti un numar!");
			return -1;
		}
	}

	@Override
	public double calculArie() {
		System.out.println("");
		System.out.println("~Calcul Arie Patrat~");
		double ariePatrat;
		
		double dimensiuneLatura = citireDate();

		while (dimensiuneLatura == -1) {
			dimensiuneLatura = citireDate(); 
		}

		ariePatrat = dimensiuneLatura * dimensiuneLatura;

		System.out.println("Aria patratului este: " + ariePatrat);
		return ariePatrat;
	}

	@Override
	public double calculPerimetru() {
		System.out.println("");
		System.out.println("~Calcul Perimetru Patrat~");
		double perimetruPatrat;
		
		double dimensiuneLatura = citireDate();

		while (dimensiuneLatura == -1) {
			dimensiuneLatura = citireDate();
		}

		perimetruPatrat = dimensiuneLatura * 4;

		System.out.println("Perimetrul patratului este: " + perimetruPatrat);
		return perimetruPatrat;
	}

}
