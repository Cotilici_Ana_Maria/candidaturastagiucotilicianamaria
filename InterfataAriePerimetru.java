
public interface InterfataAriePerimetru {
	public double calculArie();
	public double calculPerimetru();
}
