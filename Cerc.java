import java.util.InputMismatchException;
import java.util.Scanner;

public class Cerc implements InterfataAriePerimetru {

	private double citireDate() {

		try {
			Scanner s = new Scanner(System.in);
			System.out.print("Introduceti dimensiunea razei cercului: ");
			
			double valoare = s.nextDouble();

			while (valoare <= 0) {
				System.out.print("Dimensiunea trebuie sa fie > 0 ! Reintroduceti: ");
				valoare = s.nextDouble();
			}
			return valoare;
		} catch (InputMismatchException e)
		{
			System.out.println("Trebuie sa introduceti un numar!");
			return -1;
		}
	}

	@Override
	public double calculArie() {
		System.out.println("");
		System.out.println("~Calcul Arie Cerc~");
		double arieCerc;
		
		double dimensiuneRaza = citireDate();

		while (dimensiuneRaza == -1) {
			dimensiuneRaza = citireDate();
		}

		arieCerc = (Math.PI) * dimensiuneRaza * dimensiuneRaza;
		
		System.out.println("Aria cercului este: " + arieCerc);
		return arieCerc;
	}

	@Override
	public double calculPerimetru() {
		System.out.println("");
		System.out.println("~Calcul Perimetru Cerc~");
		double perimetruCerc;
		
		double dimensiuneRaza = citireDate();

		while (dimensiuneRaza == -1) {
			dimensiuneRaza = citireDate();
		}

		perimetruCerc = 2 * (Math.PI) * dimensiuneRaza;
		
		System.out.println("Perimetrul cercului este: " + perimetruCerc);
		return perimetruCerc;
	}

}
