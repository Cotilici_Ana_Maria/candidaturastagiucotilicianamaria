import java.util.InputMismatchException;
import java.util.Scanner;

public class Dreptunghi implements InterfataAriePerimetru {

	private double citireDate() {

		try {
			Scanner s = new Scanner(System.in);
			
			double valoare = s.nextDouble();

			while (valoare <= 0) {
				System.out.print("Dimensiunea trebuie sa fie > 0 ! Reintroduceti: ");
				valoare = s.nextDouble();
			}
			return valoare;
		} catch (InputMismatchException e) 
		{
			System.out.print("Trebuie sa introduceti un numar! Reintroduceti: ");
			return -1;
		}
	}

	@Override
	public double calculArie() {
		System.out.println("");
		System.out.println("~Calcul Arie Dreptunghi~");
		double arieDreptunghi;
		
		System.out.print("Introduceti lungimea dreptunghiului: ");
		
		double lungime = citireDate();
		while (lungime == -1) {
			lungime = citireDate(); 
		}

		System.out.print("Introduceti latimea dreptunghiului: ");
		
		double latime = citireDate();
		while (latime == -1) {
			latime = citireDate(); 
		}
		
		arieDreptunghi = lungime * latime;

		System.out.println("Aria dreptunghiului este: " + arieDreptunghi);
		return arieDreptunghi;
	}

	@Override
	public double calculPerimetru() {
		System.out.println("");
		System.out.println("~Calcul Perimetru Dreptunghi~");
		double perimetruDreptunghi;
		
		System.out.print("Introduceti lungimea dreptunghiului: ");
		
		double lungime = citireDate();
		while (lungime == -1) {
			lungime = citireDate(); 
		}

		System.out.print("Introduceti latimea dreptunghiului: ");
		
		double latime = citireDate();
		while (latime == -1) {
			latime = citireDate(); 
		}
		
		perimetruDreptunghi = (2*lungime) + (2*latime);

		System.out.println("Aria dreptunghiului este: " + perimetruDreptunghi);
		return perimetruDreptunghi;
	}

}
